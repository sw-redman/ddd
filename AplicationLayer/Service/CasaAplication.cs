﻿namespace AplicationLayer.Service
{
    using System.Collections.Generic;
    using AplicationLayer.Inteface;
    using DataLayer.Interface;
    using Domain;

    public class CasaAplication : ICasaAplication
    {
        private readonly IDataCasa casaData;

        public CasaAplication(IDataCasa casaData) {
            this.casaData = casaData;
        }

        public Casa AddCasa(Casa casa)
        {
           return this.casaData.AddCasa(casa);
        }

        public Casa DeleteCasa(Casa casa)
        {
            return this.casaData.DeleteCasa(casa);
        }

        public Casa GetCasa(int id)
        {
            return this.casaData.GetCasa(id);
        }

        public List<Casa> GetCasa()
        {
            return this.casaData.GetCasa();
        }

        public Casa UpdateCasa(Casa casa)
        {
            return this.casaData.UpdateCasa(casa);
        }
    }
}
