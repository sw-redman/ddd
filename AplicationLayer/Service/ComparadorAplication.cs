﻿namespace AplicationLayer.Service
{
    using System.Collections.Generic;
    using AplicationLayer.Inteface;
    using DataLayer.Interface;
    using Domain;

    public class ComparadorAplication : IComparadorAplication
    {
        private readonly IDataComprador compradorData;


        public ComparadorAplication(IDataComprador compradorData)
        {
            this.compradorData = compradorData;
        }
        
        public Comprador AddComprador(Comprador Comprador)
        {
            return this.compradorData.AddComprador(Comprador);
        }

        public Comprador DeleteComprador(Comprador Comprador)
        {
            return this.compradorData.DeleteComprador(Comprador);
        }

        public override bool Equals(object obj)
        {
            return obj is ComparadorAplication aplication &&
                   EqualityComparer<IDataComprador>.Default.Equals(this.compradorData, aplication.compradorData);
        }

        public Comprador GetComprador(int id)
        {
            return this.compradorData.GetComprador(id);
        }

        public List<Comprador> GetComprador()
        {
            return this.compradorData.GetComprador();
        }

        public Comprador UpdateComprador(Comprador Comprador)
        {
            return this.compradorData.UpdateComprador(Comprador);
        }
    }
}
