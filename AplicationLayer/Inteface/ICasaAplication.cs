﻿namespace AplicationLayer.Inteface
{
    using System.Collections.Generic;
    using Domain;

    public interface ICasaAplication
    {
        Casa GetCasa(int id);

        List<Casa> GetCasa();

        Casa DeleteCasa(Casa casa);

        Casa UpdateCasa(Casa casa);

        Casa AddCasa(Casa casa);
    }
}
