﻿namespace AplicationLayer.Inteface
{
    using System.Collections.Generic;
    using Domain;

    public interface IComparadorAplication
    {
        Comprador GetComprador(int id);

        List<Comprador> GetComprador();

        Comprador DeleteComprador(Comprador comprador);

        Comprador UpdateComprador(Comprador comprador);

        Comprador AddComprador(Comprador comprador);
    }
}
