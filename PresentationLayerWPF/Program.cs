using AplicationLayer.Inteface;
using AplicationLayer.Service;
using Context;
using DataLayer.Interface;
using DataLayer.Service;
using DTOLayer.Mapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

using System.Windows.Forms;

namespace PresentationLayerWPF
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var services = new ServiceCollection();
            ConfigureServices(services);

            using (var serviceProvider = services.BuildServiceProvider()) 
            {

                var casas = serviceProvider.GetRequiredService<Casas>();
                Application.Run(casas);

            }
        }

        private static void ConfigureServices(ServiceCollection services) 
        {
 
            // Aqui se a�ade la injeccion de dependencias
            services.AddTransient<ICasaAplication, CasaAplication>();
            services.AddTransient<IDataCasa, DataCasa>();
            services.AddTransient<Casas>();

            services.AddTransient<IComparadorAplication, ComparadorAplication>();
            services.AddTransient<IDataComprador, DataComprador>();

            services.AddTransient<IContextDb, Contex>();

            // Conxion code First ( ConnectionStrings archivo appsetting.json )
            services.AddDbContext<Contex>(options =>
               options.UseSqlServer("Server=192.168.10.81;Database=Redman.Test.CodeFirst;Persist Security Info=True;User ID=sa;Password=0s00O818;MultipleActiveResultSets=True;"));

            services.AddOptions();


            // Te injecta las DTO
            new Mapper();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        }
    }
}
