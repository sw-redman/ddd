﻿using AplicationLayer.Inteface;
using AplicationLayer.Service;
using AutoMapper;
using Domain;
using DTOLayer.DtoClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerWPF
{
    public partial class Casas : Form
    {

        private readonly ICasaAplication _casaAplciation;
        private readonly IComparadorAplication _compradorAplciation;

        private readonly IMapper _mapper;

        public Casas(ICasaAplication casaAplciation, IMapper mapper, IComparadorAplication compradorAplciation)
        {
            InitializeComponent();
            _casaAplciation = casaAplciation;
            _mapper = mapper;
            _compradorAplciation = compradorAplciation;
        }

        private void Casas_Load(object sender, EventArgs e)
        {
            List<Casa> data = this._casaAplciation.GetCasa();
            List<CasaDTO> casas = data.Select(x => this._mapper.Map<CasaDTO>(x)).ToList();

            casas.ForEach(casa =>
            {
                Comprador compr = _compradorAplciation.GetComprador(casa.CompradorID);
                dataGridView1.Rows.Add(casa.ID,casa.Calle,casa.Numero,compr.Nombre);
            });

            List<Comprador> compradores = _compradorAplciation.GetComprador();


            compradores.ForEach(comprador =>
            {
                ComboBoxCasas.Items.Add(comprador.ID+"-"+comprador.Nombre);
            });

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
