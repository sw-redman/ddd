﻿namespace DTOLayer.DtoClasses
{
    public class CasaDTO
    {
        public int ID { get; set; }

        public string Calle { get; set; }

        public int Numero { get; set; }

        public int CompradorID { get; set; }

    }
}
