﻿namespace DTOLayer.Mapper
{
    using AutoMapper;
    using Domain;
    using DTOLayer.DtoClasses;

    public class Mapper : Profile
    {
        public Mapper() {

            // Mapeas las DTO
            this.CreateMap<CasaDTO, Casa>();
            this.CreateMap<Casa ,CasaDTO>();
        }
    }
}
