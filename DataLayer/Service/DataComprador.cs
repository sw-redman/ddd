﻿namespace DataLayer.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using Context;
    using DataLayer.Interface;
    using Domain;

    public class DataComprador : IDataComprador
    {
        private readonly IContextDb context;

        public DataComprador(IContextDb context)
        {
            this.context = context;
        }

        public Comprador GetComprador(int id)
        {
            return this.context.Comprador.Where(x => x.ID == id).Single();
        }

        public List<Comprador> GetComprador()
        {
            return this.context.Comprador.ToList();
        }


        public Comprador DeleteComprador(Comprador Comprador)
        {
            this.context.Comprador.Remove(Comprador);
            this.context.SaveChanges();
            return Comprador;
        }

        public Comprador UpdateComprador(Comprador Comprador)
        {
            this.context.Comprador.Update(Comprador);
            this.context.SaveChanges();
            return Comprador;
        }

        public Comprador AddComprador(Comprador Comprador)
        {
            this.context.Comprador.Add(Comprador);
            this.context.SaveChanges();
            return Comprador;
        }
    }
}
