﻿namespace DataLayer.Service
{
    using System.Collections.Generic;
    using System.Linq;
    using Context;
    using DataLayer.Interface;
    using Domain;

    public class DataCasa : IDataCasa
    {
        private readonly IContextDb context;

        public DataCasa(IContextDb context)
        {
            this.context = context;
        }

        public Casa GetCasa(int id)
        {
            return this.context.Casa.Where(x => x.CompradorID == id).Single();
        }

        public List<Casa> GetCasa()
        {
            return this.context.Casa.ToList();
        }

        public Casa DeleteCasa(Casa casa)
        {
            this.context.Casa.Remove(casa);
            this.context.SaveChanges();
            return casa;
        }

        public Casa UpdateCasa(Casa casa)
         {
            this.context.Casa.Update(casa);
            this.context.SaveChanges();
            return casa;
        }

        public Casa AddCasa(Casa casa)
        {
            this.context.Casa.Add(casa);
            this.context.SaveChanges();
            return casa;
        }
    }
}
