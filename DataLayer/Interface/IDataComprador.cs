﻿namespace DataLayer.Interface
{
    using System.Collections.Generic;
    using Domain;

    public interface IDataComprador
    {
        Comprador GetComprador(int id);

        List<Comprador> GetComprador();

        Comprador DeleteComprador(Comprador Comprador);

        Comprador UpdateComprador(Comprador Comprador);

        Comprador AddComprador(Comprador Comprador);
    }
}
