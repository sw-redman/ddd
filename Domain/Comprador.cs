﻿namespace Domain
{
    using System.Collections.Generic;

    public class Comprador
    {
        public int ID { get; set; }

        public string Nombre { get; set; }

        public string Dni { get; set; }

        public ICollection<Casa> Casa { get; set; }

    }
}
