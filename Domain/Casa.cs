﻿namespace Domain
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Casa
    {

        public int ID { get; set; }

        public string Calle { get; set; }

        public int Numero { get; set; }
      
        public int CompradorID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime? RowVersion { get; set; }

        public Comprador Comprador { get; set; }

    }
}
