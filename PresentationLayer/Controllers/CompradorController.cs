﻿namespace PresentationLayer.Controllers
{
    using System.Collections.Generic;
    using AplicationLayer.Inteface;
    using Domain;
    using Microsoft.AspNetCore.Mvc;

    [Produces("application/json")]
    [Route("api/[controller]")]

    public class CompradorController : Controller
    {
        private readonly IComparadorAplication compradorAplication;

        public CompradorController(IComparadorAplication compradorAplication)
        {
            this.compradorAplication = compradorAplication;
        }

        // GET api/values
        [HttpGet]
        public List<Comprador> Get()
        {
            return this.compradorAplication.GetComprador();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Comprador Get(int id)
        {
            return this.compradorAplication.GetComprador(id);
        }

        // POST api/values
        [HttpPost]
        public Comprador Post([FromBody]Comprador comprador)
        {
            return this.compradorAplication.AddComprador(comprador);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public Comprador Put(int id, [FromBody]Comprador comprador)
        {
            return this.compradorAplication.UpdateComprador(comprador);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public Comprador Delete(int id, [FromBody]Comprador comprador)
        {
            return this.compradorAplication.DeleteComprador(comprador);
        }
    }
}