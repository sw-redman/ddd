﻿namespace PresentationLayer.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using AplicationLayer.Inteface;
    using AutoMapper;
    using Domain;
    using DTOLayer.DtoClasses;
    using Microsoft.AspNetCore.Mvc;

    [Produces("application/json")]
    [Route("api/[controller]")]

    public class CasaController : Controller
    {
        private readonly ICasaAplication casaAplciation;
        private readonly IMapper mapper;

        public CasaController(ICasaAplication casaAplciation, IMapper mapper) {
            this.casaAplciation = casaAplciation;
            this.mapper = mapper;
        }

        // GET: api/Casa
        [HttpGet]
        public IEnumerable<CasaDTO> Get()
        {
            var data = this.casaAplciation.GetCasa();

            var casas = data.Select(x => this.mapper.Map<CasaDTO>(x)).ToList();

            return casas;
        }

        // GET: api/Casa/5
        [HttpGet("{id}")]
        public CasaDTO Get(int id)
        {
            return this.mapper.Map<CasaDTO>(this.casaAplciation.GetCasa(id));
        }

        // POST: api/Casa
        [HttpPost]
        public Casa Post([FromBody]CasaDTO casa1)
        {
            Casa casa = this.mapper.Map<Casa>(casa1);
            return this.casaAplciation.AddCasa(casa);
        }

        // PUT: api/Casa/5
        [HttpPut()]
        public Casa Put([FromBody]CasaDTO casa2)
        {
            Casa casa = this.mapper.Map<Casa>(casa2);
            return this.casaAplciation.UpdateCasa(casa);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public Casa Delete([FromBody]CasaDTO casa2)
        {
            Casa casa = this.mapper.Map<Casa>(casa2);
            return this.casaAplciation.DeleteCasa(casa);
        }
    }
}
