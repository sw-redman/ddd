﻿namespace Context
{
    using System.Threading;
    using System.Threading.Tasks;
    using Domain;
    using Microsoft.EntityFrameworkCore;

    public interface IContextDb
    {
        DbSet<Casa> Casa { get; set; }

        DbSet<Comprador> Comprador { get; set; }

        int SaveChanges();

        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default);

    }
}
